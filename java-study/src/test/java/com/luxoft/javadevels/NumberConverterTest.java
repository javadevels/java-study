package com.luxoft.javadevels;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Ihor on 1/19/2016.
 */
public class NumberConverterTest {

    public final static double DELTA = 1 / Math.pow(10, NumberConverter.PRECISION);

    @Test
    public void testPositiveToString() throws Exception {
        assertEquals("5432345", NumberConverter.toString(5432345));
    }

    @Test
    public void testNegativeToString() throws Exception {
        assertEquals("-893691", NumberConverter.toString(-893691));
    }

    @Test
    public void testZeroToString() throws Exception {
        assertEquals("0", NumberConverter.toString(0));
    }

    @Test
    public void testPositiveToInt() throws Exception {
        assertEquals(584351, NumberConverter.toInt("584351"));
    }

    @Test
    public void testNegativeToInt() throws Exception {
        assertEquals(-92381234, NumberConverter.toInt("-92381234"));
    }

    @Test
    public void testZeroToInt() throws Exception {
        assertEquals(0, NumberConverter.toInt("0"));
    }

    @Test
    public void testLongPositiveToString() throws Exception {
        assertEquals("5432345", NumberConverter.toString(5432345L));
    }

    @Test
    public void testLongNegativeToString() throws Exception {
        assertEquals("-893691", NumberConverter.toString(-893691L));
    }

    @Test
    public void testLongZeroToString() throws Exception {
        assertEquals("0", NumberConverter.toString(0L));
    }

    @Test
    public void testDoublePositiveToString() throws Exception {
        assertEquals("3629598.264850127045", NumberConverter.toString(3629598.264850127045));
    }

    @Test
    public void testDoubleNegativeToString() throws Exception {
        assertEquals("-21544124.132564563304", NumberConverter.toString(-21544124.132564563304));
    }

    @Test
    public void testDoubleZeroToString() throws Exception {
        assertEquals("0.0", NumberConverter.toString(0.0));
    }

    @Test
    public void testPositiveToLong() throws Exception {
        assertEquals(58435123423L, NumberConverter.toLong("58435123423"));
    }

    @Test
    public void testNegativeToLong() throws Exception {
        assertEquals(-92381234234324L, NumberConverter.toLong("-92381234234324"));
    }

    @Test
    public void testZeroToLong() throws Exception {
        assertEquals(0, NumberConverter.toLong("0"));
    }

    @Test
    public void testPositiveToDouble() throws Exception {
        assertEquals(2359421.26948121, NumberConverter.toDouble("2359421.26948121"), DELTA);
    }

    @Test
    public void testNegativeToDouble() throws Exception {
        assertEquals(-5984125.125481, NumberConverter.toDouble("-5984125.125481"), DELTA);
    }

    @Test
    public void testZeroToDouble() throws Exception {
        assertEquals(0.0, NumberConverter.toDouble("0.0"), DELTA);
    }
}
