package com.luxoft.javadevels;

/**
 * Created by Ihor on 1/19/2016.
 */
public class NumberConverter {

    public final static int ZERO_CHAR = 48;
    public final static int PRECISION = 12;

    public static String toString(int value) {
        String result = "";
        int abs = Math.abs(value);
        do {
            result = (char) (ZERO_CHAR + abs % 10) + result;
            abs /= 10;
        } while (abs != 0);
        return (value < 0 ? "-" : "") + result;
    }

    public static int toInt(String string) {
        int result = 0;
        char[] chArr = string.toCharArray();
        boolean isNegative = (chArr[0] == '-');
        for (int i = isNegative ? 1 : 0; i < chArr.length; i++) {
            result += (chArr[i] - ZERO_CHAR) * Math.pow(10, chArr.length - i - 1);
        }
        return (isNegative ? -1 : 1) * result;
    }

    public static String toString(long value) {
        String result = "";

        long abs = Math.abs(value);
        do {
            result = (char) (ZERO_CHAR + abs % 10) + result;
            abs /= 10;
        } while (abs != 0);
        return (value < 0 ? "-" : "") + result;
    }

    public static String toString(double value) {
        return toString((long) value)
                + "."
                + toString((long) Math.abs(Math.pow(10, PRECISION) * (value - (long) value)));
    }

    public static long toLong(String string) {
        long result = 0;
        char[] chArr = string.toCharArray();
        boolean isNegative = (chArr[0] == '-');
        for (int i = isNegative ? 1 : 0; i < chArr.length; i++) {
            result += (chArr[i] - ZERO_CHAR) * Math.pow(10, chArr.length - i - 1);
        }
        return (isNegative ? -1 : 1) * result;
    }

    public static double toDouble(String string) {
        String[] strArr = string.split("\\.", 2);
        return (strArr[0].toCharArray()[0] == '-' ? -1 : 1) * toLong(strArr[1]) * Math.pow(10d, -strArr[1].toCharArray().length)
                + toLong(strArr[0]);
    }

    public static void main(String[] args) {
        System.out.println(toDouble("12133.123423123412"));
    }
}
