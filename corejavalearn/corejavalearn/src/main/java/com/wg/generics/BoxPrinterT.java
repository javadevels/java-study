package com.wg.generics;

/**
 * Created by Ihor on 10/23/2015.
 */
public class BoxPrinterT <T> {
    private T val;

    public BoxPrinterT(T arg) {
        val = arg;
    }

    public String toString() {
        return "BoxPrinterT {" + val + "}";
    }

    public T getValue() {
        return val;
    }
}
