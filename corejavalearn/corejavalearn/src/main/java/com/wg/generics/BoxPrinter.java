package com.wg.generics;

/**
 * Created by Ihor on 10/23/2015.
 */
public class BoxPrinter {
    private Object val;

    public BoxPrinter(Object arg) {
        val = arg;
    }

    public String toString() {
        return "BoxPrinter {" + val + "}";
    }

    public Object getValue() {
        return val;
    }
}
