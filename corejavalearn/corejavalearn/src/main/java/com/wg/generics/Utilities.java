package com.wg.generics;

import java.util.List;

/**
 * Created by Ihor on 10/23/2015.
 */
public class Utilities {
    public static <T> void fill(List<T> list, T val) {
        for (int i = 0; i < list.size(); i++)
            list.set(i, val);
    }
}
