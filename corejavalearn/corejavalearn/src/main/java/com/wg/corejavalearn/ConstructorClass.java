package com.wg.corejavalearn;

/**
 * Class shows basic principles for java Constructor
 */
public class ConstructorClass {

    /**
     * Constructor is called by default
     */
    ConstructorClass() {
        System.out.println("Object ConstructorClass has been created");
    }

    /**
     * Non-static method is calling across reference variable
     */
    public void someMethod() {
        System.out.println("Nothing odd with this method someMethod in ConstructorClass");
    }
}