package com.wg.corejavalearn;

import com.wg.generics.BoxPrinter;
import com.wg.generics.Pair;
import com.wg.generics.Utilities;
import com.wg.reusing.Bath;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static com.wg.staticimport.SimpleClassCounter.getCounter;
import static net.mindview.util.Print.print;
//import static com.wg.staticimport.SimpleClassCounter

/**
 * Main class in corejavalearn
 */
public class CoreJavaLearn {

    // JVM will assign value by default (null)
    String name;

    // Assign value implicit
    int age = 5;

    // Value by default (0)
    int gender;

    private static void changeLabel(Button btn) {
        btn.setLabel("Cancel");
    }

    /**
     * Changing reference in method -> It is not allowed to change
     *
     * @param button
     */
    private static void changeRef(Button button) {
        button = new Button("Apply");
    }

    /**
     * It is allowed to change value in array in body of method
     *
     */
    private static void changeArray(int[] array) {
        array[0] = 2;
    }

    static void printList(List<?> list) {
        for (Object l : list)
            System.out.println("{" + l + "}");
    }

    /**
     * Main method in corejavalearn
     */
    public static void main(String[] args) {
        // Creation of new object CoreJavaLearn
        // Bundle reference with type
        // Allocation memory for object
        // Attributes initialization implicit
        // Calling constructor
        // Object reference is returned by new operator
        // personInstance is variable which contain Object reference
        CoreJavaLearn personInstance = new CoreJavaLearn();

        System.out.println("==========VARIABLE INITIALIZATION============");
        // Variables in methods do not assign by default
        int genderVarMethod;
        // JVM will assign value by default to object attributes
        // attributes are named as fields in Java
        System.out.println("name = " + personInstance.name);
        System.out.println("age = " + personInstance.age);
        System.out.println("gender = " + personInstance.gender);
        // Variables in method do not initialize by default (Compiler shows error)
        //System.out.println("genderVarMethod=" + genderVarMethod);

        System.out.println("=====================CAST====================");

        double doublePrimitive = 5;
        double doublePrimitive1;
        double doublePrimitive2;
        double doublePrimitive3;
        int i1 = 3;
        int i2 = 2;
        double doubleI2 = (double)i2;
        int integerPrimitive = (int) doublePrimitive;
        int integerDevidePrimitive;
        Integer integerDevideObject = new Integer(3/2);
        System.out.println("integerPrimitive1 = " + integerPrimitive);
        short shortPrimitive = 259;
        byte bytePrimitive = (byte) shortPrimitive;
        // Explicit cast will cut data
        System.out.println("bytePrimitive = " + bytePrimitive);
        doublePrimitive1=3/2;
        doublePrimitive2=(double)3/2;
        doublePrimitive3=i1/doubleI2;
        System.out.println("doublePrimitive1 = " + doublePrimitive1);
        System.out.println("doublePrimitive2 = " + doublePrimitive2);
        System.out.println("doublePrimitive3 = " + doublePrimitive3);

        System.out.println("===========PARAMETER TRANSFORMATION==========");
        Button button = new Button("Ok");
        System.out.println(button.getLabel()); // Ok
        // It is possible to change state of object b across the reference
        changeLabel(button);
        System.out.println(button.getLabel());
        // During variable transmission is not possible to change value of original reference
        changeRef(button);
        System.out.println(button.getLabel()); // ?
        // During variable transmission it is possible to change value of array element
        int arrayIntPrimitive [] = {1};
        changeArray(arrayIntPrimitive);
        System.out.println("arrayIntPrimitive[0]=" + arrayIntPrimitive[0]);

        System.out.println("=============PARAMETER INSTANCEOF============");
        Object o = new Object();
        o = "some string";
        // operator INSTANCEOF checks compatibility types
        if (o instanceof String)
        {
            System.out.println("It's a String");
        }

        System.out.println("================COMPARE LINKS================");
        CoreJavaLearn p1 = new CoreJavaLearn();
        CoreJavaLearn p2 = new CoreJavaLearn();
        // p1 and p2 refer to different object and that's why they have different reference
        if (p1 != p2) {
            System.out.println("p1 != p2");
        }
        CoreJavaLearn p3 = p2;
        // Reference p3 creates in stack, her value -  adress of object is equal to adress p2
        if (p3 == p2) {
            System.out.println("p3 == p2");
        }
        System.out.println("=================PRIMITIVES==================");
        Integer objectIntegerMinValue = new Integer(Integer.MIN_VALUE);
        Integer objectIntegerMaxValue = new Integer(Integer.MAX_VALUE);
        System.out.println("Min=" + objectIntegerMinValue);
        System.out.println("Max=" + objectIntegerMaxValue);

        System.out.println("=================CONSTRUCTORS================");
        ConstructorClass constructorClass = new ConstructorClass();
        // Create a static object and return a reference
        // upon request.(The "Singleton" pattern):
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();
        System.out.println("singleton_1 = " + singleton1);
        System.out.println("singleton_2 = " + singleton2);
        // If Class extends super Class :: default constructor from superclass will be executed
        Descendant descendant = new Descendant();

        System.out.println("=====================ENUM====================");
        // The enum keyword allows you to create a new type with a restricted
        // set of named values, and to treat those values as regular program
        // components.
        System.out.println("EnumSuit = " + EnumSuit.DIAMOND.toString());

        System.out.println("==============INNER/NESTED CLASS=============");
        // Create  object question with inner class
        Question question = new Question();
        // Create object type1 from the inner class
        Question.Type type1 = Question.Type.SINGLE_CHOICE;
        // Calling method from outer class using reference variable to inner object
        question.setType(type1);
        // Getting reference to object under returned by method getType()
        Question.Type type = question.getType();
        System.out.println("type=" + type);
        // Casting objects
        InnerCat myCat = new InnerCat();
        OuterAnimal myAnimal = (OuterAnimal)myCat;
        // However used methods are from Inner object
        myAnimal.hide();
        myAnimal.override();

        System.out.println("==============STATIC IMPORT=================");
        // SimpleClassCounter simpleClassCounter1 = new SimpleClassCounter();
        // It is simple to use only method name when you import package with static
        System.out.println("simpleClassCounter1 = " + getCounter());
        System.out.println("simpleClassCounter2 = " + getCounter());

        System.out.println("===================GENERICS=================");

        BoxPrinter boxPrinter1 = new BoxPrinter(new Integer(10));
        System.out.println(boxPrinter1);
        //Without implicit casting will show error
        Integer integerBoxPrinter1 = (Integer) boxPrinter1.getValue();

        BoxPrinter boxPrinter2 = new BoxPrinter("Hello world");
        System.out.println(boxPrinter2);

        // Здесь программист допустил ошибку, присваивая
        // переменной типа Integer значение типа String.
//        Integer intValue2 = (Integer) value2.getValue();
        Pair<Integer, String> pair = new Pair<Integer, String>(6, " Apr");
        // Could be used easiest syntax, however in Java 7
        // Pair<Integer, String> pair = new Pair<>(6, " Apr");

        // There will be shown warning due List list2002 has unchecked assignment
        // need to put like this
        // List<String> list1001 = new LinkedList<String>();
        // WARNING
        List list1001 = new LinkedList<String>();
        list1001.add("First1001");
        list1001.add("Second1001");
        List<String> list1002 = list1001;
        for(Iterator<String> itemItr = list1002.iterator(); itemItr.hasNext();)
            System.out.println(itemItr.next());

        List<String> list2001 = new LinkedList<String>();
        list2001.add("First2001");
        list2001.add("Second2001");
        // There will be shown warning due List list2002 has unchecked assignment
        // need to put like this
        // List<String> list2002 = list2001
        // WARNING
        List list2002 = list2001;
        for(Iterator<String> itemItr = list2002.iterator(); itemItr.hasNext();) System.out.println(itemItr.next());

        List<Integer> list2003 = new ArrayList<Integer>();
        list2003.add(20031);
        list2003.add(20032);
        printList(list2003);

        List<String> strList2004 = new ArrayList<String>();
        strList2004.add("20041");
        strList2004.add("20042");
        printList(strList2004);
        // ERROR. Need to set implicit strList2004 with Integer data type
        // strList2004.add(20043);

        System.out.println("Список до обработки дженерик-методом: " + list2003);
        // Need implicit put list with Integer type, otherwise will be highlighted ERROR
        Utilities.fill(list2003, 20033);
        System.out.println("Список после обработки дженерик-методом: "
                + list2003);

        // Wildcards (Маски)

        List<?> list2005 = new ArrayList<Integer>();

        // list2005.add(new Integer(10));
        // Почему не компилируется? При использовании маски мы сообщаем компилятору,
        // чтобы он игнорировал информацию о типе, т.е. <?> - неизвестный тип.
        // При каждой попытке передачи аргументов дженерик-типа компилятор Java
        // пытается определить тип переданного аргумента. Однако теперь мы
        // используем метод add () для вставки элемента в список. При использовании
        // маски мы не знаем, какого типа аргумент может быть передан. Тут вновь видна
        // возможность ошибки, т.к. если бы добавление было возможно, то мы могли бы
        // попытаться вставить в наш список, предназначенный для чисел, строковое значение.
        // Во избежание этой проблемы, компилятор не позволяет вызывать методы, которые
        // зменяют объект. Поскольку метод add () изменяет объект, мы получаем ошибку.
        // Тем не менее есть возможность получить доступ к информации, хранящейся в объекте,
        // с использованием маски, как это было показано выше.

        // Допустим нам нужно так объявить эту переменную, чтобы она хранила только списки чисел.
        // Решение есть:

        List<? extends Number> numList2006 = new ArrayList<Integer>();

        // Аналогично ключевому слову extends в подобного рода выражениях может использоваться
        // ключевое слово super - "<? super Integer> ". Выражение <? super X> означает, что вы
        // можете использовать любой базовый тип (класс или интерфейс) типа Х, а также и сам
        // тип Х. Пара строк, которые нормально скомпилируются:

        List<? super Integer> intList2007 = new ArrayList<Integer>();
        System.out.println("The intList is: " + intList2007);

        System.out.println("===================REUSING==================");
        Bath b = new Bath();
        print(b);
    }

}

