package com.wg.corejavalearn;

/**
 * Example of outer class which has inner class  InnerCat
 */
public class OuterAnimal {

    public void hide() {
        System.out.println("The hide method in OuterAnimal");
    }

    public void override() {
        System.out.println("The override method in OuterAnimal");
    }
}
/**
 * Inner Class Cat in outer Class Animal
 */
 class InnerCat extends OuterAnimal {

    public void hide() {
        System.out.println("The hide method in InnerCat");
    }

    public void override() {
        System.out.println("The override method in InnerCat");
    }


}