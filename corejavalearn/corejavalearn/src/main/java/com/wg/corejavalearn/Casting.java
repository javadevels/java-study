package com.wg.corejavalearn;

/**
 * Class shows basic principles of casting primitive types
 */
public class Casting {

    {
        int i = 200;
        long lng = (long) i;

        // "Widening," so cast not really required
        lng = i;
        long lng2 = (long) 200;
        lng2 = 200;

        // A "narrowing conversion":
        i = (int) lng2; // Cast required
    }
}