package com.wg.corejavalearn;

/**
 * Class used as Ancestor for examples
 */
public class Ancestor {
    public Ancestor() {
        System.out.println("Ancestor constructor");
    }

    public static void test() {
        System.out.println("test");
    }
}
