package com.wg.corejavalearn;

 /**
  * Class show behavior of final and not final methods
  */
 public class FinalClass {
     /**
      * Final method could not be overwritten
      */
    final public void printFinalPublic() {
        System.out.println("writeFinalPublic");
    }
     /**
      * Non-final method could be overwritten
      */
    public void printPublic() {
        System.out.println("printPublic");
    }
}
