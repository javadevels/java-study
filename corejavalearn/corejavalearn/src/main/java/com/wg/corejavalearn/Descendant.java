package com.wg.corejavalearn;

/**
 * Class shows basic principles of java inheritance
 */
public class Descendant extends Ancestor {
    public Descendant() {
        /** super()
         *  Constructor of Ancestor is calling by default
         */
        System.out.println("Descendant constructor");

    }

}
