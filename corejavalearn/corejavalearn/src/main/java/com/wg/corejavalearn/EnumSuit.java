package com.wg.corejavalearn;

/**
 * Basic principles of Enumerations
 */
enum EnumSuit {

    // Set of objects with type enum
    DIAMOND(true), HEART(true), CLUB(false), SPADE(false);
    private boolean red;

    // Constructor
    EnumSuit(boolean b) {
        red = b;
    }

    public boolean isRed() {
        return red;
    }

    /**
     * Overriding toString method of String object
     * @return String
     */
    public String toString() {
        // Getting the name some of the EnumSuit set
        String s = name();
        s += red ? ":red" : ":black";
        return s;
    }
}