package com.wg.corejavalearn;

/**
 * Example of outer class with inner class(type enum)
 */
public class Question {
    private Type type;

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }

    // Type is inner class (enum class) under Question
    public static enum Type {
        SINGLE_CHOICE, MULIT_CHOICE, TEXT
    }
}