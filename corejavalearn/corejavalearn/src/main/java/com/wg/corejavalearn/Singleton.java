package com.wg.corejavalearn;

/**
 *
 * Class create a static object and return a reference
 * upon request.(The "Singleton" pattern)
 */

public class Singleton {
    private static final Singleton INSTANCE =
            new Singleton();

    private Singleton() {
    }

    public static final Singleton getInstance() {

        return INSTANCE;
    }
}