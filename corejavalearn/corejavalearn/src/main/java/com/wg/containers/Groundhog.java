//: containers/Groundhog.java
// Looks plausible, but doesn't work as a HashMap key.
package com.wg.containers;
package com.wg.containers;

public class Groundhog {
  protected int number;
  public Groundhog(int n) { number = n; }
  public String toString() {
    return "Groundhog #" + number;
  }
} ///:~
