import org.junit.Test;

import static org.junit.Assert.*;

public class CalculateTest {
    Calculate calculate = new Calculate();

    @Test
    public void testCalA() throws Exception {

        int n = calculate.sum(2, 2);
        assertEquals(4, n);
    }

    @Test
    public void testCalB() throws Exception {

        int n = calculate.dif(7, 2);
        assertEquals(5, n);
    }
}