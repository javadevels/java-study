import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestMoney {
    Money m1;
    Money m2;
    Money expected;
    Money result;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        m1 = new Money(12, "USD");
        m2 = new Money(14, "USD");
        expected = new Money(26, "USD");
        result = m1.add(m2);
        System.out.println(result.toString());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testAdd() {
        assertTrue(expected.equals(result));
    }
}