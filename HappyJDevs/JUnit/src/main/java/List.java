
public interface List {
    void add(Object value);

    void add(int index, Object value);

    Object set(int index, Object value);

    Object remove(int index);

    int size();

    boolean isEmpty();

    void clear();

    Object get(int index);

    Object indexOf(Object value);

    Object lastIndexOf(Object value);

    boolean contains(Object value);
}
