public class Money {

    private int value;
    private String type;

    public Money(int v, String t){
        value = v;
        type = t;
    }

    public Money add(Money m){
        return new Money(value + m.getValue(), type);
    }

    public Money div(Money m){
        return new Money(value / m.getValue(), type);
    }

    public int getValue(){
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        Money m = (Money) o;
        return m.value == this.value && m.type == this.type;
    }

    /*@Override
    public boolean equals(Object o) {
        //if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        if (value != money.value) return false;
        return !(type != null ? !type.equals(money.type) : money.type != null);

    }*/

    @Override
    public int hashCode() {
        int result = value;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
